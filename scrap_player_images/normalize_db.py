import pymongo

try:
	# Connect to mogodb and read saved files.
	# Loop through the row db to come up with a fine data output
	connection = pymongo.MongoClient("localhost", 27017)
	db = connection["squadraft"]
	collection = db["playersdata"]
	normalized_collection = db["players"]
	player_cursor = collection.find()

	for player_data in player_cursor:
		data = {
			"player": {
				"name": player_data.get("player_data").get("player").get("sname")[0],
				"avatar": player_data.get("avatar_link", None)
			},
			"team": {
				"name": (player_data.get("player_data").get("team").get("name")).strip(),
				"badge": player_data.get("badge_link", None)
			}
		}

		new_player = normalized_collection.update({"_id": player_data["_id"]}, data, True)

	# Close mongodb cursor
	player_cursor.close()

except Exception as e:
	print e
