from scrapy.item import Item, Field

class StackItem(Item):
	image_urls = Field()
	images = Field()
	player_data = Field()