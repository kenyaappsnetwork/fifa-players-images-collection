import os
import shutil
import boto
from boto.s3.connection import S3Connection
from boto.s3.key import Key
import pymongo
import urllib

AWS_ACCESS_KEY_ID = 'AKIAJOXWSEVZFZBAU5AA'
AWS_SECRET_ACCESS_KEY= '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'
AWS_REGION = 's3.eu-central-1.amazonaws.com'
BUCKET = 'fifaplayersimages'
BASE_DIR = os.path.dirname(os.path.abspath('get_player_info.py'))
LOCAL_IMAGE_TMP_STORE = os.path.join(BASE_DIR, 'dump')
AWS_IMAGE_URL = 'https://s3.eu-central-1.amazonaws.com/fifaplayersimages'

try:
	conn = S3Connection(AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY, host=AWS_REGION)
	bucket_obj = conn.get_bucket(BUCKET)

	if not os.path.exists(LOCAL_IMAGE_TMP_STORE):
		os.makedirs(LOCAL_IMAGE_TMP_STORE)

	# Connect to mogodb and read saved files.
	# Push items to s3 and update the database
	connection = pymongo.MongoClient("localhost", 27017)
	db = connection["squadraft"]
	collection = db["playersdata"]
	player_cursor = collection.find({"has_been_processed": {"$ne": True}})

	for player_data in player_cursor:
		# Create bucket key
		myfile = urllib.URLopener()
		myfile.addheaders = []
		myfile.addheader('User-Agent', 'Opera/9.80 (Windows NT 6.1; WOW64; U; de) Presto/2.10.289 Version/12.01')
		myfile.addheader('Accept-Language', 'de-DE,de;q=0.9,en;q=0.8')
		myfile.addheader('Accept', 'text/html, application/xml;q=0.9, application/xhtml+xml, image/png, image/webp, image/jpeg, image/gif, image/x-xbitmap, */*;q=0.1')

		for key, image in enumerate(player_data["image_urls"]):
			image = urllib.quote(image, safe="%/:=&?~#+!$,;'@()*[]").replace("%0D%0A","")
			filename = image.split('/')[7]
			
			myfile.retrieve(image, '%s/%s' % (LOCAL_IMAGE_TMP_STORE, filename))
			
			bkey = Key(bucket_obj)
			bkey.key = filename
			bkey.set_contents_from_filename('%s/%s' % (LOCAL_IMAGE_TMP_STORE, filename))

			# Update mongo db collection after file update
			aws_image_link = '%s/%s' %(AWS_IMAGE_URL, filename)
			if key == 0:
				player_data['avatar_link'] = aws_image_link
			if key == 1:
				player_data['badge_link'] = aws_image_link

			# Mark collection as read for normalization
			player_data['has_been_processed'] = True
			player = collection.update({"_id": player_data["_id"]}, player_data, True)

	# Close mongodb cursor
	player_cursor.close()

except Exception as e:
	print str(e)
finally:
	shutil.rmtree(LOCAL_IMAGE_TMP_STORE)
