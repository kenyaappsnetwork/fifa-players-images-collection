import scrapy
import pymongo
from items import StackItem

class fetchPlayersInfo(scrapy.Spider):
	"""How to run it
	
	Fetch players image data from futwiz.com and save it to mongodb

	DOWNLOAD_DELAY is set to hit the server every one minute

	> scrapy runspider get_player_info.py
	"""
	name = 'playerspider'
	start_urls = ['http://www.futwiz.com/en/fifa16/players?page=%s' % page for page in xrange(59,581)]

	def __init__(self):
		connection = pymongo.MongoClient("localhost", 27017)
		db = connection["squadraft"]
		self.collection = db["playersdata"]

	def parse(self, response):
		for player in response.xpath('//table[@class="table table-striped table-tdc mb-20"]/tbody/tr/td/div'):
			# print player.xpath('a/@href').extract()
			player_image = player.xpath('a/img/@src').extract()
			player_short_name = player.xpath('a/img/@alt').extract()
			player_name = player.xpath('a/strong/text()').extract()
			team_images = player.xpath('img/@src').extract()
			team = player.xpath('text()').extract()[5]

			player_data = {
				'player': {
					'name': player_name,
					'sname': player_short_name,
					'avatar': player_image[0].split('/')[5]
				},
				'team': {
					'name': team,
					'badge': team_images[1].split('/')[5]
				}
			}

			# Download images
			item = StackItem()
			item['image_urls'] = ['http://www.futwiz.com%s' %player_image[0], 'http://www.futwiz.com%s' %team_images[1]]
			item['player_data'] = player_data

			# Save to the database
			# player_data.update({'links': item['images']})
			self.collection.update({'player.sname': player_data['player']['sname']}, item , True)

			yield item




